package app.deli.com.snapble

interface IUserState {

    fun userLogin()

    fun userLogout()
}