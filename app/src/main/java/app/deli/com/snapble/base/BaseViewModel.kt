package com.example.imageloadingapplication.base.viewmodel

import androidx.lifecycle.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo

abstract class BaseViewModel : ViewModel(), LifecycleObserver {

    protected val disposables: CompositeDisposable = CompositeDisposable()

    fun Disposable.addToDisposables() {
        this.addTo(disposables)
    }

    open fun clearDisposables() {
        disposables.clear()
    }

    fun disposeOnDestroy(disposable: Disposable) {
        disposables.add(disposable)
        //info("disposeOnDestroy ViewModel, disposable size: ${disposables.size()}")
    }

    fun disposeOnDestroy(disposable: () -> Disposable) {
        disposables.add(disposable.invoke())
        //info("disposeOnDestroy ViewModel, disposable size: ${disposables.size()}")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    open fun onDestroy() {
        //info("onDestroy ViewModel, disposable size: ${disposables.size()}")
        disposables.dispose()
    }
}