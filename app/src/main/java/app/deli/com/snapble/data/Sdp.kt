package app.deli.com.snapble.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Sdp(
    @SerializedName("from_user") val from_user: String,
    @SerializedName("to_user") val to_user: String,
    @SerializedName("sdp") val sdp: String,
    @SerializedName("event_name") val event_name: String,
    @SerializedName("type") val type: String
) : Parcelable, app.deli.com.snapble.base.IExpandableChildViewHolderData {

}
