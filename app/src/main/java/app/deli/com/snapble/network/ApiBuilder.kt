package com.example.imageloadingapplication.api

import com.google.gson.GsonBuilder
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class ApiBuilder {

    companion object {
        private var mainServiceInterface: app.deli.com.snapble.network.MainServiceInterface? = null

        private lateinit var okHttpClient: OkHttpClient
        private val connectionPool = ConnectionPool()


        fun getMainWebService(): app.deli.com.snapble.network.MainServiceInterface {

            okHttpClient = buildClient(app.deli.com.snapble.util.Common.MEDIUM_NETWORK_TIME_OUT)

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val retrofit = Retrofit.Builder()
                .baseUrl("https://loremflickr.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
            mainServiceInterface = retrofit.create(app.deli.com.snapble.network.MainServiceInterface::class.java)
            return mainServiceInterface as app.deli.com.snapble.network.MainServiceInterface
        }



        private fun buildClient(timeout: Long): OkHttpClient {
            val builder = OkHttpClient.Builder()

            builder.apply {
                connectionPool(connectionPool)

                //increase timeout
                writeTimeout(timeout, TimeUnit.MILLISECONDS)
                readTimeout(timeout, TimeUnit.MILLISECONDS)
                connectTimeout(timeout, TimeUnit.MILLISECONDS)

//                cache(myCache)
//                addInterceptor(logging)
//                addInterceptor(SupportInterceptor())
//                authenticator(SupportInterceptor())
            }
            return builder.build()
        }
    }
}