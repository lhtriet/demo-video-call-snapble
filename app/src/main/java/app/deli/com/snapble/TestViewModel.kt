package app.deli.com.snapble

import android.os.AsyncTask
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import app.deli.com.snapble.util.Event
import com.example.imageloadingapplication.base.viewmodel.BaseSIViewModel
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.lang.ref.WeakReference
import java.net.Socket
import java.net.UnknownHostException


class TestViewModel : BaseSIViewModel<TestViewModel.MainViewModelState>() {

    val isStartingCall: MutableLiveData<Boolean> = MutableLiveData()
    val partnerIsBusy: MutableLiveData<Boolean> = MutableLiveData()
    val receivedOffer: MutableLiveData<String> = MutableLiveData()
    val receivedAnswer: MutableLiveData<String> = MutableLiveData()
    val receivedCandidate: MutableLiveData<Response> = MutableLiveData()

    private var publicKey: String? = null
    var thisUserId: String? = null
    var messageContent: String? = null
    var isInCall: Boolean = false
    //nokia
    var partnerUserId: String? = "5f433b7c9d2ab1d4cc05e34f"
    //iphone
//    var partnerUserId: String? = "5f40a12cd3838bfe4dd1328b"
    var authenRes: Response? = null
    private var callType: CallType = CallType.CALLER
    private var callTask: CallTask? = null
    override fun initState(): MainViewModelState {
        return MainViewModelState()
    }

    fun connectSocket(s: String) {
        callTask = CallTask(this, s)
        callTask!!.execute()
    }

    fun refreshViewModel() {
//        callTask?.closeSocket()
//        callTask?.cancel(true)
//        callTask = null
        publicKey = null
        thisUserId = null
        messageContent = null
        isInCall = false
        partnerUserId = "5f433b7c9d2ab1d4cc05e34f"
        authenRes= null
        callType = CallType.CALLER
        callTask = null
    }

    fun setPublicKey(s: String?) {
        publicKey = s
    }

    fun setCallType(s: CallType) {
        callType = s
    }

    fun sendMessage(message: String) {
//        val encriptedMessage = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            app.deli.com.snapble.util.AESUtil.encrypt(message, publicKey) + "\n"
//        } else {
//            ""
//        }
        callTask?.sendMessage(message)
    }

    fun getNewMessageFromServer(s: String) {
        if (messageContent != s) {
            messageContent = s
        Log.d("Test Activity", "Socket Receiving $s")
        try {
            val res = Response(s)
            when (res.resType) {
                "r" -> {
//                    setState { copy(isInCall = Event(true)) }
                    isStartingCall.postValue(true)
                }
                "b" -> {
                    //callee reject
                    partnerIsBusy.postValue(true)
                }
                "sdp" -> {
                    when (res.action) {
                        "offer" -> {
                            receivedOffer.postValue(res.sdp)
//                            setState { copy(receivedOffer = Event(res.sdp!!)) }
                        }
                        "answer" -> {
                            receivedAnswer.postValue(res.sdp)
//                            setState { copy(receivedAnswer = Event(res.sdp!!)) }
                        }
                    }
                }
                "candidate" -> {
                    receivedCandidate.postValue(res)
//                    setState { copy(receivedCandidate = Event(res)) }
                }
                else -> {
                    if (res.code == "400") Log.d("Test Activity", "${res.msg}")
                    else {
                        //Authen
                        authenRes = res
                        if (authenRes?.uuid != "") thisUserId = authenRes?.uuid
                        when (callType) {
                            CallType.CALLER -> {
                                if (!isInCall) {
                                    sendMessage("{'from': '${res.uuid}', 'to': '${partnerUserId}', 'type': 'i'}")
                                    isInCall = true
                                }
                            }
                            CallType.CALLEE -> {

                            }
                        }
                    }
                }
            }
        }
        catch (e: Exception) {
            Log.e("Test View Model", e.toString())
        }

        }
    }

    data class MainViewModelState(
        val isLoading: Event<Boolean> = Event(
            false
        ),
        val error: Event<Throwable>? = null,
        val isInCall: Event<Boolean>? = null,
        val receivedOffer: Event<String>? = null,
        val receivedAnswer: Event<String>? = null,
        val receivedCandidate: Event<Response>? = null,
        val isBusy: Event<BusyObject>? = null
    )
    data class BusyObject(val isBusy: Boolean = false, val fromUserId: String? = null, val toUserId: String? = null)
}


internal class CallTask(testViewModel: TestViewModel, userToken: String) : AsyncTask<String?, Void?, String?>() {
    private var resp: String? = null
    private val viewModel: WeakReference<TestViewModel> = WeakReference(testViewModel)
    private val userToken: String = userToken
    private var cl: Socket? = null
    private var pw: PrintWriter? = null
    private var mOnReceiveMessageListener: OnReceiveMessageListener? = null
    private fun connectSever(
        ip: String?,
        port: Int
    ): String? {
        var output: String? = null
        try {
            cl = Socket(ip, port) //ket noi server
            pw =
                PrintWriter(cl!!.getOutputStream(), true) //tao luong gui du lieu
//            pw!!.println("android test 123") // Gửi dữ liệu đi
            val br =
                BufferedReader(InputStreamReader(cl!!.getInputStream())) //tao luong nhan du lieu
            output = br.readLine() // Đọc dữ liệu trả về từ Server
            val testViewModel = viewModel.get()
            testViewModel?.setPublicKey(output)

//            val encriptedMessage = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                app.deli.com.snapble.util.AESUtil.encrypt("{'type': 'a', 'data' : '$USER_TOKEN'}", output) + "\n"
//            } else {
//                ""
//            }
            pw!!.println("{'type': 'a', 'data' : '$userToken'}") // Gửi dữ liệu đi

            while (br.readLine().also { output = it } != null) {
                testViewModel!!.getNewMessageFromServer(output!!)
            }

//                cl.close()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: UnknownHostException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return output //tra ve ket qua từ Server
    }

    fun closeSocket() {
        cl?.close()
    }

    fun sendMessage(message: String) {
        val runnable = Runnable {
            if (pw != null) {
                Log.d("TestViewModel", "Socket Sending: $message")
                pw!!.println(message)
                pw!!.flush()
            } else {
                Log.d("TestViewModel", "Cannot send --> mBufferOut is null")
            }
        }
        val thread = Thread(runnable)
        thread.start()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun doInBackground(vararg params: String?): String? {
        val host = "171.244.38.193"
        val port = 12761
        return connectSever(host, port)
    }

    override fun onPostExecute(s: String?) {
        super.onPostExecute(s)
//        mOnReceiveMessageListener?.onReceiveMessage(s)
        val testViewModel = viewModel.get()
        testViewModel!!.getNewMessageFromServer(s!!)
    }

    interface OnReceiveMessageListener {
        fun onReceiveMessage(
            s: String?
        )
    }
}

class Response(json: String) : JSONObject(json) {
    val fromUser: String? = this.optString("from")
    val toUser: String? = this.optString("to")
    val resType: String? = this.optString("type")
    val action: String? = this.optString("action")
    val sdp: String? = this.optString("sdp")
    val sdpMLineIndex: Int = this.optInt("sdpMLineIndex")
    val sdpMid: String? = this.optString("sdpMid")
    val msg: String? = this.optString("msg")
    val code: String? = this.optString("code")
    val uuid: String? = this.optString("uuid")
}

enum class CallType {
    CALLER, CALLEE
}