package app.deli.com.snapble.base

interface IExpandableViewHolderData

interface IExpandableChildViewHolderData:
    app.deli.com.snapble.base.IExpandableViewHolderData

interface IExpandableParentViewHolderData<CHILD_DATA_TYPE>:
    app.deli.com.snapble.base.IExpandableViewHolderData {
    var isExpanded: Boolean
    fun expandableChildren(): List<CHILD_DATA_TYPE>
}