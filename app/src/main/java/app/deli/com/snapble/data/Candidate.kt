package app.deli.com.snapble.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Candidate(
    @SerializedName("from_user") val from_user: String,
    @SerializedName("to_user") val to_user: String,
    @SerializedName("sdp") val sdp: String,
    @SerializedName("sdpMLineIndex") val sdpMLineIndex: Int,
    @SerializedName("sdpMid") val sdpMid: Int,
    @SerializedName("event_name") val event_name: String
) : Parcelable, app.deli.com.snapble.base.IExpandableChildViewHolderData {

}
