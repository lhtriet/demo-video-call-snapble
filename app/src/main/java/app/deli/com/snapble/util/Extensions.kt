package app.deli.com.snapble.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import java.io.File
import java.io.FileOutputStream
import kotlin.math.pow
import kotlin.math.roundToInt

/**
 * Get an [Event]'s value if the event is NOT NULL and its data hasn't been handled yet also
 * @param event
 * @param onEventUnhandledEvent
 */
fun <T> onEvent(event: app.deli.com.snapble.util.Event<T>?, onEventUnhandledEvent: T.() -> Unit) {
    event?.get(onEventUnhandledEvent)
}

fun File.applyRotationIfNeeded() {
    val exif = ExifInterface(this.absolutePath)
    val rotation = when (exif.getAttributeInt(
        ExifInterface.TAG_ORIENTATION,
        ExifInterface.ORIENTATION_UNDEFINED
    )) {
        ExifInterface.ORIENTATION_ROTATE_90 -> 90
        ExifInterface.ORIENTATION_ROTATE_180 -> 180
        ExifInterface.ORIENTATION_ROTATE_270 -> 270
        else -> 0
    }
    if (rotation == 0) return
    val bitmap = BitmapFactory.decodeFile(this.absolutePath)
    val matrix = Matrix().apply { postRotate(rotation.toFloat()) }
    val rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    bitmap.recycle()

    lateinit var out: FileOutputStream
    try {
        out = FileOutputStream(this)
        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
    } catch (e: Exception) {
    } finally {
        rotatedBitmap.recycle()
        out.close()
    }
}

fun Double.roundToDecimals(numDecimalPlaces: Int): Double {
    val factor = 10.0.pow(numDecimalPlaces.toDouble())
    return (this * factor).roundToInt() / factor
}