package app.deli.com.snapble.network

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Headers

interface MainServiceInterface {

    @Headers(
        value = ["Accept: application/json",
            "Content-type:application/json; charset=UTF-8"]
    )

    @GET("200/200")
    fun requestImage(): Observable<retrofit2.Response<ResponseBody>>

    //A make call
    @GET("200/200")
    fun callerWantToConnect(from: String, to: String, type: String): Observable<retrofit2.Response<ResponseBody>>

    //B accept call
    @GET("200/200")
    fun readyToConnect(from: String, type: String): Observable<retrofit2.Response<ResponseBody>>

    //B busy
    @GET("200/200")
    fun busyNow(from: String, type: String): Observable<retrofit2.Response<ResponseBody>>
}