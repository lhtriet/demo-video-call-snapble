package app.deli.com.snapble.util

/**
 * Used as a wrapper for data that is exposed via a LiveData that represents an event.
 */
open class Event<out T>(private val content: T) {

    var hasBeenHandled = false
        private set // Allow external read but not write

    /**
     * Returns the content and prevents its use again.
     */
    fun getDataIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    /**
     * Returns the data, event if it's already been handled.
     * @return <T>
     */
    fun peekData(): T = content

    /**
     * @see peekData
     */
    operator fun invoke() = peekData()

    fun get(onEventUnhandledData: T.() -> Unit) {
        getDataIfNotHandled()?.let { onEventUnhandledData(it) }
    }
}