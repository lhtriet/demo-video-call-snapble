package app.deli.com.snapble.sharePreference

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import app.deli.com.snapble.util.Constants

class AppSharePreference(val context: Context) {

    private val PREFS_NAME = "AppSharePreference"
    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun getSharedPreferences(): SharedPreferences {
        return sharedPreferences
    }

    /**
     * setIsInCallStatus, save to share preference
     * @param accessToken String
     */
    fun setIsInCallStatus(isInCall: Boolean) {
        sharedPreferences.edit {
            putBoolean(Constants.IS_IN_CALL_STATUS, isInCall)
            //info("set access token successful")
        }
    }

    /**
     * getIsInCallStatus
     *
     * @return IsInCallStatus (boolean)
     */
    fun getIsInCallStatus(): Boolean {
        return sharedPreferences.getBoolean(Constants.IS_IN_CALL_STATUS, false)!!
    }
}