package app.deli.com.snapble.util

class Common {

    companion object {

        const val FAST_NETWORK_TIME_OUT: Long = 1000 * 10 // default fast time out

        const val MEDIUM_NETWORK_TIME_OUT: Long = 1000 * 20 //default medium time out

        const val LONG_NETWORK_TIME_OUT: Long = 1000 * 30 //default medium time out

    }
}