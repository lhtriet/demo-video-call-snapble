package app.deli.com.snapble.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.os.StrictMode
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import app.deli.com.snapble.CallType
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.Socket
import java.net.UnknownHostException


class MessagingService(): Service() {
    private var userToken: String? = null
    private var callType: CallType? = null
    private var cl: Socket? = null
    private var pw: PrintWriter? = null
    val host = "27.118.22.2"
    val port = 12761
    companion object {
        fun newIntent(
            context: Context,
            callType: CallType,
            userToken: String
        ) = Intent(context, MessagingService::class.java).apply {
            putExtra("CALL_TYPE", callType)
            putExtra("USER_TOKEN", userToken)
        }
    }
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.let {
            userToken = it.getSerializableExtra("USER_TOKEN") as String?
            callType = it.getSerializableExtra("CALL_TYPE") as CallType?
        }
        val policy =
            StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        var output: String? = null
        try {
            cl = Socket(host, port) //ket noi server
            pw =
                PrintWriter(cl!!.getOutputStream(), true) //tao luong gui du lieu
            val br = BufferedReader(InputStreamReader(cl!!.getInputStream())) //tao luong nhan du lieu
            output = br.readLine() // Đọc dữ liệu trả về từ Server

            pw!!.println("{'type': 'a', 'data' : '$userToken'}\n") // Gửi dữ liệu đi
//            output = br.readLine()
//            receivedMessage(output)
            while (br.readLine().also { output = it } != null) {
                receivedMessage(output)
            }


        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: UnknownHostException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return super.onStartCommand(intent, flags, startId)
    }

    fun sendMessage(message: String) {
        val runnable = Runnable {
            if (pw != null) {
                Log.d("TestViewModel", "Sending: $message")
                pw!!.println(message)
                pw!!.flush()
            } else {
                Log.d("TestViewModel", "Cannot send --> mBufferOut is null")
            }
        }
        val thread = Thread(runnable)
        thread.start()
    }


    override fun onBind(intent: Intent?): IBinder? {
        TODO("Not yet implemented")
    }

    private fun receivedMessage(s: String?) {
        val sendLevel = Intent()
        sendLevel.action = "GET_MESSAGE"
        sendLevel.putExtra("MESSAGE", s)
        sendBroadcast(sendLevel)
    }
}