package app.deli.com.snapble.data

enum class ScreenState {
    INCALL,
    ANSWERING,
    DIALLING,
    ENDED,
    BUSY,
    STANDBY,
    DECLINE
}