package app.deli.com.snapble.service

import android.R
import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.lifecycle.MutableLiveData
import app.deli.com.snapble.TestActivity
import app.deli.com.snapble.TestViewModel
import app.deli.com.snapble.sharePreference.AppSharePreference
import app.deli.com.snapble.util.Event
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject


class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // ...

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: ${remoteMessage.from}")

        val notification = Notification(remoteMessage.data.values.elementAt(0))

        if (!AppSharePreference(applicationContext).getIsInCallStatus()) {
            if (notification.message == "inint_call") {
                val intent = Intent(this, TestActivity::class.java)
                intent.putExtra("CALL_TYPE", "ANSWERING")
                intent.putExtra("FROM_USER_ID", notification.payload?.data?.from)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                val builder = NotificationCompat.Builder(this)
                builder
                    .setContentTitle("Incoming Call")
                    .setContentText(notification.payload?.data?.from + " is calling you")
                    .setSmallIcon(R.mipmap.sym_def_app_icon)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            }
        } else {
            TestViewModel.MainViewModelState(isBusy = Event(TestViewModel.BusyObject(isBusy = true, fromUserId = notification.payload?.data?.from, toUserId = notification.payload?.data?.to)))
            val alertDialog: AlertDialog.Builder = AlertDialog.Builder(this)
            alertDialog.setTitle("AlertDialog")
            alertDialog.setMessage("You just declined a call because you are busy.")
            alertDialog.setPositiveButton(
                "yes"
            ) { _, _ ->
            }
//            val alert: AlertDialog = alertDialog.create()
//            alert.setCanceledOnTouchOutside(true)
//            alert.show()
        }

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: ${remoteMessage.data}")

        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d(TAG, "Message Notification Body: ${it.body}")

            val builder = NotificationCompat.Builder(this)
            builder
                .setContentTitle("Title")
                .setContentText("content")
                .setSmallIcon(R.mipmap.sym_def_app_icon)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

}

class Notification(json: String) : JSONObject(json) {
    val message: String? = this.optString("message")
    val timestamp: Long? = this.optLong("timestamp")
    val payload: Payload = Payload(this.optString("payload"))
}
data class Payload(val json: String): JSONObject(json) {
    val data: Data = Data(this.optString("data"))
//    val data = this.optJSONArray("data") as Data
}
class Data(json: String): JSONObject(json) {
    val from: String? = this.optString("from")
    val to: String? = this.optString("to")
    val type: String? = this.optString("type")
}