package app.deli.com.snapble.data

enum class CallStatus {
    STARTED,
    ACCEPTED,
    ENDED,
    REQUESTING,
    RINGING
}