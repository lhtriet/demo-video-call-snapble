package com.example.imageloadingapplication.base.viewmodel

import androidx.lifecycle.MutableLiveData

abstract class BaseSIViewModel<S> : BaseViewModel() {

    val state: MutableLiveData<S> = MutableLiveData()

    abstract fun initState(): S

    /**
     * Get current state, [initState] will be returned if current state is null
     */
    open fun state() = state.value ?: initState()

    open fun postState(stateFunc: S.() -> S) {
        state.postValue(stateFunc(state.value ?: initState()))
    }

    open fun setState(stateFunc: S.() -> S) {
        state.value = stateFunc(state.value ?: initState())
    }

    open fun withState(stateConsumer: S.() -> Unit) {
        stateConsumer(state.value ?: initState())
    }
}